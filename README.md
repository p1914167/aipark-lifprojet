Dans le cadre de notre troisième année de la licence de l’Université Claude Bernard Lyon 1, il nous est donné le choix d’un projet d’une durée d’environ 3 mois afin d’acquérir des nouvelles connaissances et de l’expérience. Nous avons décidé de développer un programme qui réalisera un parking intelligent pour le stationnement des véhicules.

L’équipe est composée de Rémi Dijoux, Mohamed Bangoura et Mamadou Diallo.

➔ Images : contient toutes les images nécessaires pour l’affichage de notre programme graphiquement.  
➔ src : contient tous les fichiers .h et .cpp.  
➔ out  : contient les fichiers exécutables que notre programme a compilé .  
➔ connecte : contient les classes pour accéder à la base de données.  
➔ controleur : contient le contrôleur pour gérer les actions  effectuée de l’interface graphique (MVC).  
➔ entites :  contient l’ensemble de nos entités (voitures, camions, etc…).  
➔ externe :  contient des bibliothèques externes au projet.  
➔ model : contient le traitement back-end et le serveur pour la mise de en place de la discussion entre le véhicule et le parking.  
➔ observateur : contient l’interface pour les classes graphiques (vue).  
➔ observer : contient le l’interface pour le modèle.  
➔ outils : contient l’ensemble des éléments statique du programme (taille de la fenêtre, enum, etc…).  
➔ vues : contient l’ensemble des fenêtres graphiques.  
